module.exports = function(sequelize, Sequelize) {
    var UserSchema = sequelize.define('user', {
        email: Sequelize.TEXT,
        password: Sequelize.TEXT
    }, {
        timestamps: false
    });
    return UserSchema;
}