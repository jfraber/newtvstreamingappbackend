module.exports = function(sequelize, Sequelize) {
    var UserDataSchema = sequelize.define('userdata', {
        data_type: Sequelize.TEXT,
        data_values: Sequelize.TEXT
    }, {
        timestamps: false
    });
    return UserDataSchema;
}