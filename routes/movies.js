var express = require('express');
const Sequelize = require('sequelize');
const op = Sequelize.Op;
var Movies = require('../models').Movies;
var router = express.Router();

router.get('/search', function(req, res) {
    console.log('getting one book');
    let page = 0;
    let pageSize = 25;
    const offset = page * pageSize
    const limit = offset + pageSize

    return Movies.findAll({
        limit,
        offset,
        // where: {
        //     tid: {
        //         [op.like]: '%' + req.query.q + '%'
        //     }
        // },
        attributes: ['movieId', 'tid', 'title', 'wordsInTitle', 'url', 'imdbRating', 'ratingCount', 'duration', 'year', 'type', 'nrOfWins']
    }).then(movies => {
        res.json(movies);
    })
});


module.exports = router;